import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from './src/store/configureStore';
import App from './src/components/main/main';
import './styles.scss';

const store = configureStore();

// eslint-disable-next-line
render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
