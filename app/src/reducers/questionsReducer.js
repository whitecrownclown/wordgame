import initialState from './initialState';
import { RECEIVE_QUESTIONS } from '../actions/actionTypes';

export default function questions(state = initialState.questions, action) {
  switch (action.type) {
    case RECEIVE_QUESTIONS:
      return {
        ...state,
        model: action.payload.questions,
      };
    default:
      return state;
  }
}
