import RECEIVE_QUESTIONS from './actionTypes';
import data from '../utils/data';

export function receiveQuestions(questions) {
  return { type: RECEIVE_QUESTIONS, payload: { questions } };
}

export function fetchQuestions() {
  return dispatch => dispatch(receiveQuestions(data));
}
