const questions = [
  {
    id: 1,
    title: 'Vul het ontbrekende woord in',
    question: '{0} ben Stefan.',
    image: 'Group 7.png',
    answers: [
      {
        value: 'ik',
        valid: true,
      },
      {
        value: 'jij/je',
      },
    ],
  },
  {
    id: 2,
    title: '',
    question: '',
    image: '',
    answers: [
      {
        value: 'this one',
        valid: true,
      },
      {
        value: 'not this one',
      },
    ],
  },
];

export default questions;
