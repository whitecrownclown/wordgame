import React from 'react';
import Question from '../question/question';
import Header from '../header/header';

class App extends React.Component {
  render() {
    return [<Header />, <Question />];
  }
}

export default App;
