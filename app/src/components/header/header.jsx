import React from 'react';
import { connect } from 'react-redux';
import { number } from 'prop-types';

import './header.scss';
import logo from '../../assets/desktop/Logo.svg';

import { Desktop } from '../../utils/media';

function mapStateToProps(state) {
  return {
    current: state.questions.current,
    total: state.questions.model ? state.questions.model.length : state.questions.current,
  };
}

@connect(mapStateToProps)
class Header extends React.Component {
    static propTypes = {
      total: number,
      current: number,
    };

    static defaultProps = {
      total: 1,
      current: 1,
    };

    render() {
      const { total, current } = this.props;

      return (
        <div className="header">
          <div className="group logo">
            <img src={logo} alt="logo" />
            <Desktop>
              <span id="theme">Thema 3 : Familie</span>
            </Desktop>
          </div>
          <div className="group current">
            <span className="step">
                        Afbeelding {current} van de {total}
            </span>
          </div>
          <div className="group score">
            <span className="correct">0</span>
            <span className="separator" />
            <span className="incorrect">0</span>
            <button className="button right">&#10006;</button>
          </div>
        </div>
      );
    }
}

export default Header;
