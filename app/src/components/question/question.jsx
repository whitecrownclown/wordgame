import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { arrayOf, number, shape, string, bool, func } from 'prop-types';
import classnames from 'classnames';

import { currentQuestionSelector } from './questionSelector';
import * as actions from '../../actions/actions';

import './question.scss';

const SPACE = ' ';
const PLACEHOLDER = '{0}';

function mapStateToProps(state) {
  return {
    current: currentQuestionSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

function requireImage(image) {
  // eslint-disable-next-line
    return image ? require(`../../assets/mobile/${image}`) : '';
}

const questionShape = shape({
  id: number,
  title: string,
  question: string,
  image: string,
  answers: arrayOf(shape({
    value: string,
    valid: bool,
  })),
});

@connect(mapStateToProps, mapDispatchToProps)
class Question extends React.Component {
    static propTypes = {
      actions: shape({
        fetchQuestions: func,
      }),
      current: questionShape,
    };

    static defaultProps = {
      actions: { fetchQuestions: () => {} },
      current: {
        image: '',
        title: '',
        question: '',
        answers: [],
      },
    };

    constructor() {
      super();

      this.state = {
        answer: '',
      };
    }

    componentDidMount() {
      this.props.actions.fetchQuestions();
    }

    onChange = (answer) => {
      this.setState({
        answer,
      });
    };

    onKeyPress = (e, answer) => {
      if (e.keyCode === 13 || e.which === 13) {
        this.onChange(answer);
      }
    };

    displayAnswers = answers =>
      answers.map(({ value }) => {
        const isActive = this.state.answer === value;
        return (
          <div
            tabIndex={0}
            onKeyPress={(e) => {
                        this.onKeyPress(e, value);
                    }}
            role="button"
            className={classnames('answer', isActive && 'active')}
            key={value}
            onClick={() => this.onChange(value)}
          >
            {value}
          </div>
        );
      });

    buildAnswer = question =>
      question.split(SPACE).map((part) => {
        if (part === PLACEHOLDER) {
          return <span>{this.state.answer}</span>;
        }
        return `${SPACE}${part}`;
      });

    render() {
      const {
        current: {
          image, title, question = '', answers = [],
        } = {},
      } = this.props;

      return (
        <div className="question">
          <div className="questionHeader">
            <p className="title">{title}</p>
          </div>
          <div className="questionBody">
            <img src={requireImage(image)} alt="questionImage" />
            <p className="selectedAnswer">{this.buildAnswer(question)}</p>
            <div className="answerList">{this.displayAnswers(answers)}</div>
          </div>
        </div>
      );
    }
}

export default Question;
