import { createSelector } from 'reselect';

export function questionsSelector(state) {
  return state.questions.model;
}

export function currentIdSelector(state) {
  return state.questions.current;
}

// eslint-disable-next-line
export const currentQuestionSelector = createSelector(questionsSelector, currentIdSelector, (questions, currentId) =>
  questions.find(question => question.id === currentId));
